class Movie < ApplicationRecord

	def display_name
    	"#{title}"
 	 end

	validates :title, presence: true,
                   	  length: { minimum: 5 }

	has_one :crawl
	has_and_belongs_to_many :characters
end
