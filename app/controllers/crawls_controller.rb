class CrawlsController < ApplicationController

	def index
		@crawls = Crawl.order(:movie_id)
	end

	def show
		@crawl = Crawl.find(params[:id])
	end

	def new
		@crawl = Crawl.new
	end

	def edit
		@crawl = Crawl.find(params[:id])
	end

	def create
		@crawl = Crawl.new(movie_params)

		if @crawl.save
		 	redirect_to @crawl
		else
			render 'new'
		end
	end

	def update
		@crawl = Crawl.find(params[:id])

		if @crawl.update(crawl_params)
			redirect_to @crawl
		else
			render 'edit'
		end
	end

	def destroy
		@crawl = Crawl.find(params[:id])
		@crawl.destroy

		redirect_to crawls_path
	end


	private

	def crawl_params
		params.require(:crawl).permit(:text, :movie_id)
	end

end
