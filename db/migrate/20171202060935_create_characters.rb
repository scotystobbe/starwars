class CreateCharacters < ActiveRecord::Migration[5.1]
  def change
    create_table :characters do |t|
      t.string :name
      t.string :race
      t.string :planet
      t.integer :actor_id
      t.string :image_url

      t.timestamps
    end
  end
end
