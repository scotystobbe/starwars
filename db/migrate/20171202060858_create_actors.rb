class CreateActors < ActiveRecord::Migration[5.1]
  def change
    create_table :actors do |t|
      t.string :name
      t.date :birthdate
      t.string :imdb_link

      t.timestamps
    end
  end
end
