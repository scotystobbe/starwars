class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :title
      t.date :release_date
      t.integer :order
      t.integer :length
      t.integer :crawl_id

      t.timestamps
    end
  end
end
