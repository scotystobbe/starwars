class AddColumnMovieIdToCrawls < ActiveRecord::Migration[5.1]
  def change
  	add_column :crawls, :movie_id, :integer
  end
end
